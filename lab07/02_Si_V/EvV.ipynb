{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Energy vs Volume"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This set of commands will import the various libraries will be using.\n",
    "# You'll need to be sure to evaluate this cell or the rest of the\n",
    "# code in this notebook won't work.\n",
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "import scipy.optimize"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Getting the Free Energy from DFT\n",
    "\n",
    "Calculate the free energy as the DFT Total Energy plus the Vibrational Energy:\n",
    "\n",
    "$$\n",
    "F=[E_{\\mathrm{DFT}} + E_{\\mathrm{vib}}] - TS\n",
    "$$\n",
    "\n",
    "The first thing we need is the DFT total energy vs volume. You have calculated this, and we'll now import the data and analyse it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "dft_EvV = np.loadtxt(\"etot_v_vol.dat\")\n",
    "\n",
    "# This plots the first column as the x-axis\n",
    "# and the second column as the y-axis.\n",
    "plt.plot(dft_EvV[:, 0], dft_EvV[:, 1])\n",
    "plt.xlabel(\"Volume (Bohr^3)\")\n",
    "plt.ylabel(\"Total Energy (Ry)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's fit this with an equation of state. A good one to use is the [Birch-Murnaghan](https://en.wikipedia.org/wiki/Birch%E2%80%93Murnaghan_equation_of_state) equation of state which usually fits very well, and gives the equilibrium volume, bulk modulus and pressure derivative of the bulk modulus directly as fit parameters. This gives energy as a function of volume as\n",
    "\n",
    "$$\n",
    "E(V) = E_0 + \\frac{9 V_0 B_0}{16}\\left\\{\n",
    "\\left[\\left(\\frac{V_0}{V}\\right)^{2/3}-1\\right]^3 B'_0 +\n",
    "\\left[\\left(\\frac{V_0}{V}\\right)^{2/3}-1\\right]^2\n",
    "\\left[6 - 4\\left(\\frac{V_0}{V}\\right)^{2/3}\\right]\n",
    "\\right\\}.\n",
    "$$\n",
    "\n",
    "We'll need to first define a python function that uses this definiation to return the energy as a function of the volume and the various fit parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def BirchMurnaghanE(V, E0, V0, B0, BP):\n",
    "    '''Birch-Murnaghan Equation of State for E(V).'''\n",
    "    E = E0 + 9*V0*B0/16 * (BP*((V0/V)**(2/3) - 1)**3 +\n",
    "                           ((V0/V)**(2/3) - 1)**2 * (6-4*(V0/V)**(2/3)))\n",
    "    return E"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we can pass that function, along with our data to the `curve_fit` function from scipy. We'll need to make initial guesses for $E0$ and $V0$ though, but these can be estimated from our plot above, or we can take the minimum energy value from our input data and the volume at which it occurs."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "E0_guess = dft_EvV[:, 1].min()\n",
    "V0_guess = dft_EvV[:, 0][np.argmin(dft_EvV[:, 1])]\n",
    "\n",
    "# This returns both the fit paramters as an array and the covariance matrix.\n",
    "BMparams, BMcovariance = scipy.optimize.curve_fit(BirchMurnaghanE,\n",
    "                                                  dft_EvV[:, 0],\n",
    "                                                  dft_EvV[:, 1],\n",
    "                                                  p0=[E0_guess, V0_guess, 1, 1])\n",
    "\n",
    "print(\"E0 = \", BMparams[0])\n",
    "print(\"V0 = \", BMparams[1])\n",
    "print(\"B0 = \", BMparams[2])\n",
    "print(\"BP = \", BMparams[3])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, this gives us an equilibrium volume (in Bohr$^3$). The value of _B0_\n",
    "will be in units of Energy/Volume. If we want to compare to the experimental\n",
    "value we'd need to convert to a more standard unit. 1 Ry is 2.179872325E-18\n",
    "J, and 1 Bohr$^3$ is $(5.2917721067\\times10^{-11})^3$ m$^3$.\n",
    "\n",
    "- Try converting our fit bulk modulus to GPa."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now let's see how good a fit this is, by plotting it together with our DFT results:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xplt = np.linspace(dft_EvV[:, 0].min(), dft_EvV[:, 0].max(), 100)\n",
    "yplt = BirchMurnaghanE(xplt, *BMparams)\n",
    "plt.plot(dft_EvV[:, 0], dft_EvV[:, 1], 'bo')\n",
    "plt.plot(xplt, yplt, 'r-')\n",
    "plt.xlabel(\"Volume (Bohr^3)\")\n",
    "plt.ylabel(\"Energy (Ry)\")\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This looks pretty good. Now we can move on to looking at the free energy."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.7"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
